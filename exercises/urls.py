"""exercises URL Configuration"""

from django.contrib import admin
from django.urls import include, path

from authentication import views as authentication
from user_area import views as user_area
from blog import views as blog

urlpatterns = [
    path('admin/', admin.site.urls),
    path('contacts/', include('contacts.urls')),

    path('posts/', blog.posts_list, name="posts_list"),
    path('post/<int:post_pk>/', blog.post_details, name="post_details"),

    path('accounts/login/', authentication.Login.as_view(), name="accounts_login"),
    path('accounts/create/', authentication.CreateUser.as_view(),
         name="accounts_create"),
    path('accounts/logout/', authentication.Logout.as_view(), name="accounts_logout"),

    path('account/dashboard/', user_area.dashboard, name="user_area_dashboard")

]
