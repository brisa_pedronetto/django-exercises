""" Authentication views """
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from django.views import View


class CreateUser(View):
    """ Create User """

    def get(self, request):
        """ Create user page """
        if request.user.is_authenticated:
            return redirect('user_area_dashboard')

        return render(request, 'create_user.html')

    def post(self, request):
        """ Do create user """
        post = request.POST

        if User.objects.filter(username=post.get('email')).exists():
            context = {'error': 'Username exists'}
            return render(request, 'create_user.html', context)

        User.objects.create_user(
            post.get('email'), post.get('email'), post.get('password'))

        return redirect('accounts_login')


class Login(View):
    """ Login """

    def get(self, request):
        """ Login page """
        if request.user.is_authenticated:
            return redirect('user_area_dashboard')

        return render(request, 'login.html')

    def post(self, request):
        """ Do login """
        post = request.POST

        user = authenticate(username=post.get('username'),
                            password=post.get('password'))

        if user is not None:
            login(request, user)
            return redirect('user_area_dashboard')
        else:
            context = {'error': 'Login error'}
            return render(request, 'login.html', context)


class Logout(View):
    """ Logout class """

    def get(self, request):
        """ Do logout """
        logout(request)
        return redirect('accounts_login')
