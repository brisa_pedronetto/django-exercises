""" User Area views """
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, reverse


@login_required
def dashboard(request):
    """ Dashboard """
    return render(request, 'dashboard.html')
