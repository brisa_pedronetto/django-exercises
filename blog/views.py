from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader

from .models import Post


def posts_list(request):
    """ Posts list without shortcut """
    all_posts = Post.objects.all()
    template = loader.get_template('posts_list.html')
    context = {
        'all_posts': all_posts
    }
    return HttpResponse(template.render(context, request))


def post_details(request, post_pk):
    """ Posts list with shortcut """
    post = Post.objects.get(pk=post_pk)
    context = {
        'post': post
    }
    return render(request, 'post_details.html', context)
