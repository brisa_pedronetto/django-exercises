""" Contact list module """
import json
import sys


class ContactManager():
    """ ContactManager Class """

    def __init__(self):
        """ Initialize the app
        Here we try to open the contacts.json file
        and decode it's JSON file into the contacts
        list but if that fails (except) we'll use an
        empty array to represent contacts list
        """
        try:
            with open('contacts.json', 'r') as file:  # r is for "read"
                content = file.read()
        except FileNotFoundError:
            self.contacts = []
        else:
            self.contacts = json.loads(content)

    def prompt(self):
        """ Ask user what should be the action
        Here we use a dictionary to store the
        possible commands, which is the "pythonic"
        way to do a "switch", common in PHP or Javascript
        """
        available_commands = {
            'add': self.add,
            'update': self.update,
            'list': self.list,
            'exit': self.exit,
        }

        command = input('What do you want to do? (add, update, list, exit) ')
        print('')

        # lambda in python is an anonymoud function, in this case
        # used to provide a fallback to the get() method
        # https://www.w3schools.com/python/python_lambda.asp
        action = available_commands.get(command, lambda: 'Invalid command')
        action()

        # Keep asking after the command is processed (unless it's
        # killed in self.exit())
        self.prompt()

    def add(self):
        """ Add a new contact
        Here we just add a new item to the self.contacts array
        TODO: check if the contact already exists and fail if so
        """
        name = input('Contact name: ')
        email = input('Contact email: ')
        print('')

        new_contact = {
            'name': name,
            'email': email
        }
        self.contacts.append(new_contact)

        # Save the new contact in contacts.json
        self.save()

        # \n means "new line" and it's only used here to
        # space out the result of this print from the next
        print('-> Added: {} / {}\n'.format(new_contact.get('name'),
                                           new_contact.get('email')))

    def update(self):
        """ Update an existing contact """
        email = input('Inform the contact\'s email: ')

        # Check if user exists and fail if it doesn't
        filtered_contacts = [
            contact.get('email')
            for contact in self.contacts
            if contact.get('email') == email
        ]
        if not filtered_contacts:
            print('Contact not found\n')
            # Stop the function from going further
            return 0

        # Select first result
        # (TODO: should be the only one)
        contact = filtered_contacts[0]

        new_name = input('New name: ')
        new_email = input('New email: ')
        print('')

        updated_contact = {
            'name': new_name,
            'email': new_email
        }

        # Find the right contact once more and this time
        # update it with a new dictionary
        for contact in self.contacts:
            if contact.get('email') == email:
                contact.update(updated_contact)

        # Save to file...
        self.save()

        print('-> Updated: {} / {}\n'.format(updated_contact.get('name'),
                                             updated_contact.get('email')))

    def list(self):
        """ List contacts
        The simples method in this class. We'll just
        iterate the existing contacts and print their information
        """
        for contact in self.contacts:
            print('-> {} / {}\n'.format(contact.get('name'),
                                        contact.get('email')))

    def save(self):
        """ Save contact
        Here we open the contacts.json file again, but this
        time with the "w" flag, meaning "write". We also use
        the + sign which makes python create the file if it
        doesn't exist yet. Once opened, we'll just overwrite
        whatever content this file has with our new JSON string.
        In case we needed to actually add to the file instead of
        overwriting we'd use the flag "a+" instead of "w+"
        """
        with open('contacts.json', 'w+') as file:
            contacts_json = json.dumps(self.contacts)
            file.write(contacts_json)

    def exit(self):
        """ Exit script """
        sys.exit()


# Create ContactManager instance
contact_manager = ContactManager()
# Start asking what does the user want to do
contact_manager.prompt()
