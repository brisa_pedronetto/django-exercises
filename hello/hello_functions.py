""" Contact list module """
import sys


class App():
    """ App Class """
    contacts = []
    answer = ''

    def ask(self):
        """ Ask user what should be the action """
        available_commands = {
            'add': self.add,
            'list': self.list,
            'exit': self.exit,
        }

        command = input('What do you want to do? (add, list, exit) ')
        print('')

        action = available_commands.get(command, lambda: 'Invalid command')
        action()

        self.ask()

    def add(self):
        """ Add a new contact """
        name = input('Contact name: ')
        email = input('Contact email: ')
        print('')

        new_contact = {
            'name': name,
            'email': email
        }
        self.contacts.append(new_contact)

        print('-> Added: %s / %s' %
              (new_contact.get('name'), new_contact.get('email')))
        print('')

    def list(self):
        """ List contacts """
        for contact in self.contacts:
            print('-> %s / %s' % (contact.get('name'), contact.get('email')))
        print('')

    def exit(self):
        """ Exit app """
        sys.exit()


app = App()
app.ask()
