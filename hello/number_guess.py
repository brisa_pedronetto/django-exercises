""" Mini guessing game """
import random

while True:
    choice = input('From 1 to 5, what is the number I am thinking about? ')
    if (int(choice) == random.randrange(1, 6)):
        print('Right!\n')
    else:
        print('Wrong...\n')
