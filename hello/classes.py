class Animal():
    name = 'Animal'

    def walk(self):
        print('{} walking...'.format(self.name))


class Dog(Animal):
    name = 'Dog'

    def bark(self):
        print('{} barking'.format(self.name))


class Cat(Animal):
    name = 'Cat'

    def meow(self):
        print('{} meowing =^.^='.format(self.name))


dog = Dog()
dog.bark()
dog.walk()

cat = Cat()
cat.meow()
cat.walk()
