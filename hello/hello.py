""" Contact list module """

command = ''
contacts = []

while True:
    answer = input('What do you want to do? (add, list, exit) ')

    if answer == 'add':
        name = input('Contact name: ')
        email = input('Contact email: ')

        new_contact = {
            'name': name,
            'email': email
        }
        contacts.append(new_contact)

        print('Added: %s / %s' %
              (new_contact.get('name'), new_contact.get('email')))
        continue

    if answer == 'list':
        for contact in contacts:
            print('%s / %s' % (contact.get('name'), contact.get('email')))
        continue

    if answer == 'exit':
        print('Exiting...')
        break

    else:
        print()
