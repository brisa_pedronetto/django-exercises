""" Fun times with characters
Usage:
$ python3 screen.py [length_of_output]
"""
import sys
from time import sleep

text = '!^-.-^!#!^-.-^!'
char_count = int(sys.argv[1]) if (len(sys.argv) > 1) else 10000

while True:
    for i in range(1, len(text)):
        to_print = text[0:i]
        times = round(char_count / len(to_print))
        print(to_print * times)
        sleep(0.2)
    for i in reversed(range(1, (len(text) - 1))):
        to_print = text[:i]
        times = round(char_count / len(to_print))
        print(to_print * times)
        sleep(0.2)
