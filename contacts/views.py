""" Contacts Views """

from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from .forms import ContactForm
from .models import Contact


class ContactList(ListView):
    """Contact view"""

    model = Contact
    template_name = 'contact_list.html'


class ContactDetail(DetailView):
    """Contact view"""

    model = Contact
    template_name = 'contact_details.html'


class ContactCreate(CreateView):
    """Contact view"""

    form_class = ContactForm
    template_name = 'contact_form.html'

    def get_success_url(self):
        pk = self.object.pk
        return reverse_lazy('contact_detail', kwargs={'pk': pk})


class ContactUpdate(UpdateView):
    """Contact view"""

    model = Contact
    form_class = ContactForm
    template_name = 'contact_form.html'

    def get_success_url(self):
        pk = self.object.pk
        return reverse_lazy('contact_detail', kwargs={'pk': pk})


class ContactDelete(DeleteView):
    """Contact view"""

    model = Contact
    template_name = 'contact_confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('contact_list')
